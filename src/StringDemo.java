/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class StringDemo {

    public static void main(String[] args) {
        
    }
    
    public static void main3(String[] args) {
        //String s = "java programming language";
        
        String s = "java";        
        System.out.println(s);
        System.out.println(s.hashCode());
        System.out.println(System.identityHashCode(s));
        
        String s2 = "java";
        System.out.println(s2);
        System.out.println(s2.hashCode());
        System.out.println(System.identityHashCode(s2));
        
        String s3 = new String("java");
        System.out.println(s3);
        System.out.println(s3.hashCode());
        System.out.println(System.identityHashCode(s3));
        
        s2 = s2 + " language";
        System.out.println(s2);
        System.out.println(s2.hashCode());
        System.out.println(System.identityHashCode(s2));
    }
    
    public static void main2(String[] args) {
        String s = "java programming language";

        char c = 'a';
        int count = 0;

        for (int i = 0; i < s.length(); i++) {
            count++;
            if (s.charAt(i) == c) {
                System.out.println(i);
            }
        }
        System.out.println("Count " + count);

        count = 0;
        int index = -1;

        do {
            count++;
            index = s.indexOf(c, index+1);
            
            if(index != -1){
                System.out.println(index);
            }
        } while (index != -1);

        System.out.println("Count " + count);
    }

    public static void main1(String[] args) {
        String s = "java programming language";

        System.out.println(s);
        System.out.println(s.length());
        System.out.println(s.charAt(6));
        System.out.println(s.contains("ram"));
        System.out.println(s.contains("rom"));

        System.out.println(s.indexOf("ram"));
        System.out.println(s.indexOf("rom"));

        System.out.println(s.indexOf("ng"));
        System.out.println(s.lastIndexOf("ng"));

        System.out.println(s.indexOf('a'));
        System.out.println(s.lastIndexOf('a'));
        System.out.println(s.indexOf('g', s.indexOf('g') + 1));

    }
}
