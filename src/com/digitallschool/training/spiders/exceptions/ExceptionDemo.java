/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.exceptions;

import com.digitallschool.training.spiders.inheritance.DeveloperTask;
import com.digitallschool.training.spiders.inheritance.Employee;
import com.digitallschool.training.spiders.inheritance.Task;
import com.digitallschool.training.spiders.inheritance.TeamLeadTask;
import com.digitallschool.training.spiders.inheritance.TeamLeader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ExceptionDemo {

    public static void main(String[] args) {
        TeamLeader t1 = new TeamLeader(12, "Dennis", "Sea", "Unix");
        
        t1.assignTask(new Task(10, "Prepare project report"));
        t1.assignTask(new DeveloperTask(12, "Code file open algorithm"));
        t1.assignTask(new TeamLeadTask(14, "Recruit a new member for the team"));
        
        System.out.println(t1.taskSize());
    }
    
    public static void main3(String[] args) {
        Employee e1 = new Employee(15, "Dennis");
        
        e1.assignTask(new Task(1278, "Some Assigned work"));
        e1.assignTask(new Task(1296, "Work on employee induction"));
        
        System.out.println(e1.taskSize());
        
        e1.assignTask(new Task(1416, "Take the interview"));
        System.out.println(e1.taskSize());
    }
    
    public static void main2(String[] args) {
        int i = Integer.parseInt("1012", 2);
        System.out.println(i);
    }
    
    public static void main1(String[] args) {
        FileReader in = null;

        try {
            in = new FileReader("C:\\Users\\rkvod\\Desktop\\multi.txtx");
            int v;

            while ((v = in.read()) != -1) {
                System.out.print((char) v);
            }

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {

        } finally {
            try {
                /*if (in != null) {
                    in.close();
                }*/

                if (Objects.nonNull(in)) {
                    in.close();
                }
            } catch (IOException ex) {

            }
        }

    }
}
