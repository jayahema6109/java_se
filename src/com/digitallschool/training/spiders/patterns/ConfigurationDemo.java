/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.patterns;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ConfigurationDemo {

    public static void main(String[] args) throws IOException {
        Configuration conf = Configuration.getInstance();

        System.out.println(conf.getCustomersSizePerScreen());
        System.out.println(conf.getPaymentsSizePerScreen());
        System.out.println(conf.getPurchasesSizePerScreen());

        if (conf.getCustomersSizePerScreen() < 20) {
            conf.setCustomersSizePerScreen(20);
        }
        if (conf.getPaymentsSizePerScreen() < 20) {
            conf.setPaymentsSizePerScreen(20);
        }

        if (conf.getPurchasesSizePerScreen() < 20) {
            conf.setPurchasesSizePerScreen(20);
        }

        System.out.println("#################");
        System.out.println(conf.getCustomersSizePerScreen());
        System.out.println(conf.getPaymentsSizePerScreen());
        System.out.println(conf.getPurchasesSizePerScreen());

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Output screen size for Customers: ");
        conf.setCustomersSizePerScreen(Integer.parseInt(input.readLine()));

        Configuration.saveConfig();
    }
}
