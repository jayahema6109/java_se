/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.ashraf;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ChatService {
    public static void main(String[] args) throws Exception{
        ServerSocket server = new ServerSocket(2015);
        
        while(true){
            Socket client = server.accept();
            System.out.println(client.getRemoteSocketAddress());
            String ip = client.getRemoteSocketAddress().toString();
            
            PrintWriter out = new PrintWriter(client.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            
            System.out.println("Ready to read from client ....");
            String clientInput = in.readLine();
            System.out.println(ip + " " + clientInput);
            if(clientInput.equals("HI")){
                out.println("HELLO");
            }
        }
        
        
        //server.close();
    }
}
