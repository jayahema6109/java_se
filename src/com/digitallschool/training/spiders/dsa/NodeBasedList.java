/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.dsa;

import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class NodeBasedList {

    private Node head;
    private int size;

    {
        head = null;
        size = 0;
    }

    public Object remove(int index) {
        if (size == 0 || index > size - 1 || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Node temp = head;

        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return removeNode(temp);

        /*if (index == 0) {
            return removeHead(head);
        } else {
            Node temp = head;
            for (int i = 0; i < index; i++) {
                temp = temp.next;
            }
            return removeNode(temp);
        }*/
 /*Node temp = head;

        if (index == 0) {
            head = head.next;
            temp.next = null;
            if (Objects.nonNull(head)) {
                head.previous = null;
            }
        } else {
            for (int i = 0; i < index; i++) {
                temp = temp.next;
            }

            if (temp.next != null) {
                temp.previous.next = temp.next;
                temp.next.previous = temp.previous;
                temp.next = null;
                temp.previous = null;
            } else {
                temp.previous.next = null;
                temp.previous = null;
            }
        }

        size--;
        Object value = temp.data;
        temp.data = null;
        return value;*/
    }

    public boolean add(Object e) {
        Node temp = new Node(null, null, e);

        if (isEmpty()) {
            head = temp;
            size++;
        } else {
            Node handle = head;
            while (handle.next != null) {
                handle = handle.next;
            }
            handle.next = temp;
            temp.previous = handle;
            size++;
        }

        return true;
    }

    public boolean isEmpty() {
        return (head == null) ? true : false;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder();
        temp.append("[");

        Node handle = head;
        if (handle != null) {
            temp.append(handle.data);
            handle = handle.next;

            while (handle != null) {
                temp.append(", " + handle.data);
                handle = handle.next;
            }
        }

        temp.append("]");
        return temp.toString();
    }

    private Object removeNode(Node now) {
        if (Objects.isNull(now.previous)) {
            return removeHead(now);
        } else if (Objects.isNull(now.next)) {
            return removeTail(now);
        }

        now.previous.next = now.next;
        now.next.previous = now.previous;
        now.next = null;
        now.previous = null;
        Object temp = now.data;
        now.data = null;
        size--;
        return temp;
    }

    private Object removeHead(Node now) {
        head = now.next;
        if (Objects.nonNull(now)) {
            head.previous = null;
        }
        now.next = null;
        Object temp = now.data;
        now.data = null;
        size--;
        return temp;
    }

    private Object removeTail(Node now) {
        now.previous.next = null;
        now.previous = null;
        Object temp = now.data;
        now.data = null;
        size--;
        return temp;
    }

    private static class Node {

        private Node previous;
        private Node next;
        private Object data;

        private Node(Node previous, Node next, Object data) {
            this.previous = previous;
            this.next = next;
            this.data = data;
        }
    }
}
