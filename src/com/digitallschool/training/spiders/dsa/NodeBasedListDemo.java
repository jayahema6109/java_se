/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.dsa;


/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class NodeBasedListDemo {

    public static void main(String[] args) {
        NodeBasedList t = new NodeBasedList();

        System.out.println(t.isEmpty());

        t.add(20);
        t.add(30);
        t.add(40);
        System.out.println(t);

        System.out.println(t.remove(1));
        System.out.println(t);
        t.add(50);
        System.out.println(t);
    }
}
