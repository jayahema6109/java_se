/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.menudrivenproject;

import java.time.LocalDate;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Customer implements Comparable<Customer>{

    private Integer customerId;
    private String name;
    private String mobile;
    private String address;
    private LocalDate addedOn;
    private Double pendingAmount;
    
    @Override
    public int compareTo(Customer y){
        return this.getCustomerId() - y.getCustomerId();
    }

    public Customer(Integer customerId, String name, String mobile, String address, LocalDate addedOn,
            Double pendingAmount) {
        setCustomerId(customerId);
        setName(name);
        setMobile(mobile);
        setAddress(address);
        setAddedOn(addedOn);
        setPendingAmount(pendingAmount);
    }
    
    @Override
    public String toString(){
        return "[" + customerId + ", " + name + ", " + mobile + ", "
                + pendingAmount + "]";
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(LocalDate addedOn) {
        this.addedOn = addedOn;
    }

    public Double getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(Double pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

}
