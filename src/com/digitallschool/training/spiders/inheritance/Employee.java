/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

import com.digitallschool.training.spiders.exceptions.TaskOverflowException;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Employee extends Object implements Comparable{

    @Override
    public int compareTo(Object t){
        Employee that = (Employee)t;
        
        //return this.employeeId - that.employeeId;
        
        return this.name.compareTo(that.name);
    }
    
    private int employeeId;
    private String name;
    private String address;
    private String email;
    private String mobile;

    private final int TASK_LIMITS;
    private Task[] tasks;

    {
        TASK_LIMITS = 4;
        tasks = new Task[TASK_LIMITS];
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(employeeId, name);
    }

    @Override
    public boolean equals(Object t) {
        Employee that = (Employee) t;

        if (this.employeeId == that.employeeId
                && this.name.equals(that.name)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Employee[" + employeeId + " : " + name + "]";
    }

    public Employee(int employeeId, String name) throws IllegalArgumentException {
        setEmployeeId(employeeId);
        setName(name);
    }

    public Employee(int employeeId, String name, String address, String email, String mobile)
            throws IllegalArgumentException {
        setEmployeeId(employeeId);
        setName(name);
        setAddress(address);
        setEmail(email);
        setMobile(mobile);
    }

    public void assignTask(Task task) throws TaskOverflowException {
        boolean assigned = false;

        for (int i = 0; i < tasks.length; i++) {
            if (Objects.isNull(tasks[i])) {
                tasks[i] = task;
                assigned = true;
                break;
            }
        }

        if (!assigned) {
            throw new TaskOverflowException("No scope for new Tasks");
        }
    }

    public int taskSize() {
        int count = 0;
        for (Task t : tasks) {
            if (Objects.nonNull(t)) {
                count++;
            } else {
                break;
            }
        }

        return count;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) throws IllegalArgumentException {
        if (employeeId > 0) {
            this.employeeId = employeeId;
        } else {
            throw new IllegalArgumentException("Invalid id for Employee: " + employeeId);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
