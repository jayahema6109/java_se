/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class TeamLeader extends Developer {

    private String projectName;
    private Developer[] developers;

    public TeamLeader(int employeeId, String name, String technology, String projectName) {
        //super(employeeId, name, technology);
        this(employeeId, name, technology, projectName, null);
        //projectName = pn;
        //setProjectName(projectName);
    }

    public TeamLeader(int employeeId, String name, String technology, String projectName, Developer[] developers) {
        super(employeeId, name, technology);
        //projectName = pn;
        //developers = devs;
        setProjectName(projectName);
        setDevelopers(developers);
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Developer[] getDevelopers() {
        return developers;
    }

    public void setDevelopers(Developer[] developers) {
        this.developers = developers;
    }

}
