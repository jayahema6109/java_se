/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ObjectDemo {

    public static void main(String[] args) {
        Employee e1 = new Employee(10, "Johnson");
        Employee e2 = new Employee(10, "Johnson");
        
        if(e1 == e2){
            System.out.println("Both are same");
            System.out.println(e1.hashCode() + " " + e2.hashCode());
        }else{
            System.out.println("They are not same");
            System.out.println(e1.hashCode() + " " + e2.hashCode());
        }
        
        if(e1.equals(e2)){
            System.out.println("Both are same");
            System.out.println(e1.hashCode() + " " + e2.hashCode());
        }else{
            System.out.println("They are not same");
            System.out.println(e1.hashCode() + " " + e2.hashCode());
        }
    }
    
    public static void main3(String[] args) {
        Employee e1 = new Employee(10, "Johnson");
        
        System.out.println(e1);
        System.out.println(e1.toString());
        
        System.out.println(e1.hashCode());
        System.out.println(Integer.toHexString(e1.hashCode()));
        System.out.println(e1.getClass().getName());
        
        System.out.println(e1.getClass().getName() + "@" + Integer.toHexString(e1.hashCode()));
    }

    public static void main2(String[] args) throws IOException {
        List<String> empLines = Files.readAllLines(Paths.get("C:\\Users\\rkvod\\Desktop\\sample\\emp_data.csv"));

        Employee[] employees = new Employee[empLines.size()];

        for (int i = 0; i < employees.length; i++) {
            String[] values = empLines.get(i).split(",");

            employees[i] = new Employee(
                    Integer.parseInt(values[0]),
                    values[1], values[2], values[3], values[4]
            );
        }

        for (int i = 0; i < 79; i++) {
            System.out.print("+");
        }
        System.out.println("");
        for (Employee e : employees) {
            //System.out.println(e.getEmployeeId() + " | " + e.getName() + " | "
            //      + e.getAddress() + " | " + e.getEmail() + " | " + e.getMobile());

            System.out.printf("| %4d | %-12s | %-15s | %-20s | %12s |%n",
                    e.getEmployeeId(), e.getName(), e.getAddress(), e.getEmail(), e.getMobile());
        }
        for (int i = 0; i < 79; i++) {
            System.out.print("+");
        }
        System.out.println("");
    }

    public static void main1(String[] args) {
        Employee e1 = new Employee(10, "Johnson");

        System.out.println(e1);
        System.out.println(e1.toString());
    }
}
