/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class SavingAccount extends Account {

    public static double minimumBalance;

    static {
        minimumBalance = 1000;
    }

    public SavingAccount() {
        super(minimumBalance);
    }

    public SavingAccount(double amount) {
        super(Math.max(Math.abs(amount), minimumBalance));
    }

    @Override
    public void credit(double amount) {
        this.balance += Math.abs(amount);
    }

    @Override
    public void debit(double amount) {
        amount = Math.abs(amount);
        if (balance - amount >= 0) {
            balance -= amount;
        } else {
            System.out.println("Insufficient balance error");
        }
    }

}
