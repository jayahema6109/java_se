/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.streams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class BufferedStreamsDemo {

    public static void main(String[] args) {

        try (
                BufferedInputStream in = new BufferedInputStream(new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\HowInTheHell.jpg"));
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream("C:\\Users\\rkvod\\Desktop\\sample\\HowInTheHell_3.jpg"));) {

            int v;

            LocalDateTime start = LocalDateTime.now();
            while ((v = in.read()) != -1) {
                out.write(v);
            }
            System.out.println(ChronoUnit.MILLIS.between(start,
                    LocalDateTime.now()));

            System.out.println("File copied successfully");
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        }

    }
}
