/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.streams;

import com.digitallschool.training.spiders.Car;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class StreamsDemo {

    public static void main(String[] args) {
        try (ObjectInputStream in = new ObjectInputStream(
                new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\obj_data.txt"))) {

            Object temp = in.readObject();
            Car c = (Car)temp;
            
            System.out.println(c.BRAND);
            System.out.println(c.model);
            System.out.println(c.getEngine().getPrice());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }catch(ClassNotFoundException cnfe){
            
        }
    }
    
    public static void main3(String[] args) {
        Car c1 = new Car("TATA", "Nexon", 850000, new Engine());
        //c1.setEngine(new Engine());

        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("C:\\Users\\rkvod\\Desktop\\sample\\obj_data.txt"))) {

            out.writeObject(c1);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void main2(String[] args) {
        FileInputStream in = null;

        try {
            in = new FileInputStream("C:\\Users\\rkvod\\Desktop\\multi.txt");

            int data = in.read();
            //in.close();
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        } finally {
            /*if (Objects.nonNull(in)) {
            try {
                in.close();
            } catch (IOException ioe) {

            }
             */
            try {
                if (Objects.nonNull(in)) {
                    in.close();
                }
            } catch (IOException ioe) {

            }

        }
    }

}
