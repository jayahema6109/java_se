/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

import java.time.LocalDateTime;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class StaticDemo {
    static LocalDateTime one = getDate();
    static LocalDateTime two;
    static LocalDateTime three;
    
    static{
        two = getDate();
    }
    
    public static void main(String[] args) {
        three = getDate();
        
        System.out.println("################");
        System.out.println(one);
        System.out.println(two);
        System.out.println(three);
    }
    
    static LocalDateTime getDate(){
        try{
            Thread.sleep(2000);
        }catch(InterruptedException ie){
            
        }
        
        LocalDateTime temp = LocalDateTime.now();
        System.out.println(temp);
        return temp;
    }
}
